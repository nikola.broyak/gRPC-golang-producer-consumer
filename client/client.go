package main

import (
	"context"
	"io"
	"log"
	"time"

	msg "producer_consumer_grpc/prod_cons"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

func main() {
	var opts []grpc.DialOption
	opts = append(opts, grpc.WithTransportCredentials(insecure.NewCredentials()))
	conn, err := grpc.Dial("localhost:40000", opts...)
	if err != nil {
		log.Fatalf("fail to dial: %v", err)
	}
	defer conn.Close()
	client := msg.NewStreamMsgsServiceClient(conn)

	streamMessages(client)

}

func streamMessages(client msg.StreamMsgsServiceClient) {
	log.Printf("starting Stream Messages Func at Client")
	ctx := context.Background()
	log.Print(ctx)
	stream, err := client.StreamMessagesFunc(ctx)
	if err != io.EOF && err != nil {
		log.Fatalf("client.StreamMessagesFunc failed: %v", err)
	}
	for {
		message, err := stream.Recv()
		stream.Send(message)
		time.Sleep(5000 * time.Millisecond)
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatalf("client.StreamMessagesFunc failed: %v", err)
		}
		log.Printf("Message id: %v, text: %v", message.Id, message.Msg)

	}

}
