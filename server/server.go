package main

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"net"
	"os"
	"os/exec"
	"path"
	msg "producer_consumer_grpc/prod_cons"
	"strconv"
	"strings"
	"time"

	"google.golang.org/grpc"
)

func main() {
	log.Print("start")
	lis, err := net.Listen("tcp", "localhost:40000")
	if err != nil {
		log.Fatalf("Failed to listen: %v", err)
	}
	var opts []grpc.ServerOption
	grpcServer := grpc.NewServer(opts...)
	msg.RegisterStreamMsgsServiceServer(grpcServer, newServer())
	log.Print("server created on localhost:40000")
	grpcServer.Serve(lis)

}

type streamMsgsServiceServer struct {
	msg.UnimplementedStreamMsgsServiceServer
	messages *msg.Messages
}

func (s *streamMsgsServiceServer) loadMessages() {
	pwd, _ := os.Getwd()
	if len(os.Args) == 1 {
		log.Fatal("No ID file specified")
	} else {
		filepath := path.Join(pwd, os.Args[1])
		s.messages = processArgFile(filepath)
	}
}
func newServer() *streamMsgsServiceServer {
	s := &streamMsgsServiceServer{}
	return s
}

func (s *streamMsgsServiceServer) StreamMessagesFunc(stream msg.StreamMsgsService_StreamMessagesFuncServer) error {
	s.loadMessages()
	msgChan := make(chan *msg.Message, 5)
	for _, message := range s.messages.Msg {
		if len(msgChan) == cap(msgChan) {
			log.Print("Channel full, waiting for receiving client")
			for {
				_, err := stream.Recv()
				if err == io.EOF {
					return nil
				}
				if err != nil {
					return err
				}
				log.Print("Client receive successful, freeing up channel and inserting next message")
				_ = <-msgChan
				break
			}

		}
		time.Sleep(100 * time.Millisecond)
		log.Printf("\nsending message:\n\t\t%v", message)
		msgChan <- message
		if err := stream.Send(message); err != nil {
			return err
		}
	}
	return nil
}
func populateIDsSlice(file *os.File, linecount int) *msg.Messages {
	scanner := bufio.NewScanner(file)
	msgs := &msg.Messages{}

	for i := 0; i < linecount; i++ {
		scanner.Scan()
		msgs.Msg = append(msgs.Msg, &msg.Message{Id: fmt.Sprintf("%v", i), Msg: scanner.Text()})
	}
	return msgs
}

func getLineCount(filepath string) int {
	output, _ := exec.Command("wc", "-l", filepath).Output()
	stringoutput := strings.Split(string(output), " ")[0]
	linecount, _ := strconv.Atoi(stringoutput)
	return linecount
}

func processArgFile(filepath string) *msg.Messages {
	file, err := os.Open(filepath)
	if err != nil {
		log.Fatal("Incorrect file name")
	}
	linecount := getLineCount(filepath)
	msgs := populateIDsSlice(file, linecount)
	if len(msgs.Msg) == 0 {
		log.Fatal("Empty IDs file, aborting")
	}
	return msgs
}
