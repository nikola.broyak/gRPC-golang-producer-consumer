// - protoc-gen-go-grpc v1.2.0
// - protoc             v3.21.5
// source: prod_cons.proto

package prod_cons

import (
	context "context"

	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

// StreamMsgsServiceClient is the client API for StreamMsgsService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type StreamMsgsServiceClient interface {
	StreamMessagesFunc(ctx context.Context, opts ...grpc.CallOption) (StreamMsgsService_StreamMessagesFuncClient, error)
}

type streamMsgsServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewStreamMsgsServiceClient(cc grpc.ClientConnInterface) StreamMsgsServiceClient {
	return &streamMsgsServiceClient{cc}
}

func (c *streamMsgsServiceClient) StreamMessagesFunc(ctx context.Context, opts ...grpc.CallOption) (StreamMsgsService_StreamMessagesFuncClient, error) {
	stream, err := c.cc.NewStream(ctx, &StreamMsgsService_ServiceDesc.Streams[0], "/prod_cons.StreamMsgsService/StreamMessagesFunc", opts...)
	if err != nil {
		return nil, err
	}
	x := &streamMsgsServiceStreamMessagesFuncClient{stream}
	return x, nil
}

type StreamMsgsService_StreamMessagesFuncClient interface {
	Send(*Message) error
	Recv() (*Message, error)
	grpc.ClientStream
}

type streamMsgsServiceStreamMessagesFuncClient struct {
	grpc.ClientStream
}

func (x *streamMsgsServiceStreamMessagesFuncClient) Send(m *Message) error {
	return x.ClientStream.SendMsg(m)
}

func (x *streamMsgsServiceStreamMessagesFuncClient) Recv() (*Message, error) {
	m := new(Message)
	if err := x.ClientStream.RecvMsg(m); err != nil {
		return nil, err
	}
	return m, nil
}

// StreamMsgsServiceServer is the server API for StreamMsgsService service.
// All implementations must embed UnimplementedStreamMsgsServiceServer
// for forward compatibility
type StreamMsgsServiceServer interface {
	StreamMessagesFunc(StreamMsgsService_StreamMessagesFuncServer) error
	mustEmbedUnimplementedStreamMsgsServiceServer()
}

// UnimplementedStreamMsgsServiceServer must be embedded to have forward compatible implementations.
type UnimplementedStreamMsgsServiceServer struct {
}

func (UnimplementedStreamMsgsServiceServer) StreamMessagesFunc(StreamMsgsService_StreamMessagesFuncServer) error {
	return status.Errorf(codes.Unimplemented, "method StreamMessagesFunc not implemented")
}
func (UnimplementedStreamMsgsServiceServer) mustEmbedUnimplementedStreamMsgsServiceServer() {}

// UnsafeStreamMsgsServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to StreamMsgsServiceServer will
// result in compilation errors.
type UnsafeStreamMsgsServiceServer interface {
	mustEmbedUnimplementedStreamMsgsServiceServer()
}

func RegisterStreamMsgsServiceServer(s grpc.ServiceRegistrar, srv StreamMsgsServiceServer) {
	s.RegisterService(&StreamMsgsService_ServiceDesc, srv)
}

func _StreamMsgsService_StreamMessagesFunc_Handler(srv interface{}, stream grpc.ServerStream) error {
	return srv.(StreamMsgsServiceServer).StreamMessagesFunc(&streamMsgsServiceStreamMessagesFuncServer{stream})
}

type StreamMsgsService_StreamMessagesFuncServer interface {
	Send(*Message) error
	Recv() (*Message, error)
	grpc.ServerStream
}

type streamMsgsServiceStreamMessagesFuncServer struct {
	grpc.ServerStream
}

func (x *streamMsgsServiceStreamMessagesFuncServer) Send(m *Message) error {
	return x.ServerStream.SendMsg(m)
}

func (x *streamMsgsServiceStreamMessagesFuncServer) Recv() (*Message, error) {
	m := new(Message)
	if err := x.ServerStream.RecvMsg(m); err != nil {
		return nil, err
	}
	return m, nil
}

// StreamMsgsService_ServiceDesc is the grpc.ServiceDesc for StreamMsgsService service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var StreamMsgsService_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "prod_cons.StreamMsgsService",
	HandlerType: (*StreamMsgsServiceServer)(nil),
	Methods:     []grpc.MethodDesc{},
	Streams: []grpc.StreamDesc{
		{
			StreamName:    "StreamMessagesFunc",
			Handler:       _StreamMsgsService_StreamMessagesFunc_Handler,
			ServerStreams: true,
			ClientStreams: true,
		},
	},
	Metadata: "prod_cons.proto",
}
